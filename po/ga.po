# Irish translations for gnome-maps package.
# Copyright (C) 2013 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-maps package.
# Seán de Búrca <leftmostcat@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-maps.master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-08-30 22:31-0600\n"
"PO-Revision-Date: 2013-08-30 22:37-0600\n"
"Last-Translator: Seán de Búrca <leftmostcat@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : "
"4;\n"

#: ../data/gnome-maps.desktop.in.in.h:1 ../src/application.js:53
#: ../src/mainWindow.js:331 ../src/main-window.ui.h:3
msgid "Maps"
msgstr "Léarscáileanna"

#: ../data/gnome-maps.desktop.in.in.h:2
msgid "A simple maps application"
msgstr "Feidhmchlár simplí léarscáileanna"

#: ../data/gnome-maps.desktop.in.in.h:3
msgid "Maps;"
msgstr "Léarscáileanna;"

#: ../data/org.gnome.maps.gschema.xml.in.h:1
msgid "Window size"
msgstr "Méid na fuinneoige"

#: ../data/org.gnome.maps.gschema.xml.in.h:2
msgid "Window size (width and height)."
msgstr "Méid na fuinneoige (leithead agus airde)."

#: ../data/org.gnome.maps.gschema.xml.in.h:3
msgid "Window position"
msgstr "Ionad na fuinneoige"

#: ../data/org.gnome.maps.gschema.xml.in.h:4
msgid "Window position (x and y)."
msgstr "Ionad na fuinneoige (x agus y)."

#: ../data/org.gnome.maps.gschema.xml.in.h:5
msgid "Window maximized"
msgstr "Uasmhéadaíodh an fhuinneog"

#: ../data/org.gnome.maps.gschema.xml.in.h:6
msgid "Window maximized state"
msgstr "Staid uasmhéadaithe na fuinneoige"

#: ../data/org.gnome.maps.gschema.xml.in.h:7
msgid "Last known location and accuracy"
msgstr ""

#: ../data/org.gnome.maps.gschema.xml.in.h:8
msgid ""
"Last known location (latitude and longitude in degrees) and accuracy (in "
"meters)."
msgstr ""

#: ../data/org.gnome.maps.gschema.xml.in.h:9
msgid "Description of last known location"
msgstr ""

#: ../data/org.gnome.maps.gschema.xml.in.h:10
msgid "Description of last known location of user."
msgstr ""

#: ../data/org.gnome.maps.gschema.xml.in.h:11
msgid "Maximum number of search results"
msgstr ""

#: ../data/org.gnome.maps.gschema.xml.in.h:12
msgid "Maximum number of search results from geocode search."
msgstr ""

#: ../src/app-menu.ui.h:1
msgid "About"
msgstr "Maidir Leis Seo"

#: ../src/app-menu.ui.h:2
msgid "Quit"
msgstr "Scoir"

#: ../src/mainWindow.js:330
msgid "translator-credits"
msgstr "Seán de Búrca <leftmostcat@gmail.com>"

#: ../src/mainWindow.js:332
msgid "A map application for GNOME"
msgstr "Feidhmchlár léarscáile do GNOME"

#: ../src/main-window.ui.h:1
msgid "Street"
msgstr "Sráid"

#: ../src/main-window.ui.h:2
msgid "Satellite"
msgstr "Satailít"

#: ../src/mapLocation.js:121
msgid " km²"
msgstr " km²"

#: ../src/userLocation.js:58
#, javascript-format
msgid ""
"%s\n"
"Position Accuracy: %s"
msgstr ""
"%s\n"
"Cruinneas Ionaid: %s"
