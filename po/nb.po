# Norwegian bokmål translation of gnome-maps.
# Copyright (C) 2013 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Kjartan Maraas <kmaraas@gnome.org>, 2013-2015.
# Åka Sikrom <a4NOSPAMPLEASETHANKYOU@hush.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-maps 3.17.x\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-07 19:51+0200\n"
"PO-Revision-Date: 2015-09-07 19:52+0200\n"
"Last-Translator: Kjartan Maraas <kmaraas@gnome.org>\n"
"Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: Norwegian bokmål\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#: ../data/org.gnome.Maps.appdata.xml.in.h:1
msgid ""
"Maps gives you quick access to maps all across the world. It allows you to "
"quickly find the place you’re looking for by searching for a city or street, "
"or locate a place to meet a friend."
msgstr ""
"Kart gir deg rask tilgang til kart for hele verden. Det lar deg finne stedet "
"du leter etter ved å søke etter en by eller en gate, eller finne et sted å "
"møte en venn."

#: ../data/org.gnome.Maps.appdata.xml.in.h:2
msgid ""
"Maps uses the collaborative OpenStreetMap database, made by hundreds of "
"thousands of people across the globe."
msgstr ""
"Kart bruker OpenStreetMap-databasen som er laget av hundretusener av "
"mennesker fra hele verden."

#. Translators: Search is carried out on OpenStreetMap data using Nominatim.
#. Visit http://wiki.openstreetmap.org/wiki/Nominatim/Special_Phrases and click
#. your language to see what words you can use for the translated search.
#: ../data/org.gnome.Maps.appdata.xml.in.h:6
msgid ""
"You can even search for specific types of locations, such as “Pubs near Main "
"Street, Boston” or “Hotels near Alexanderplatz, Berlin”."
msgstr ""
"Du kan også søke etter en bestemt type sted, som f.eks. «Pub nær Main "
"Street, Boston» eller «Hoteller nær Alexanderplatz, Berlin»."

#. Translators: This is the program name.
#: ../data/org.gnome.Maps.desktop.in.h:1 ../data/ui/main-window.ui.h:1
#: ../src/application.js:77 ../src/mainWindow.js:367
msgid "Maps"
msgstr "Kart"

#: ../data/org.gnome.Maps.desktop.in.h:2
msgid "A simple maps application"
msgstr "Et enkelt kartprogram"

#: ../data/org.gnome.Maps.desktop.in.h:3
msgid "Maps;"
msgstr "Kart;"

#: ../data/org.gnome.Maps.gschema.xml.h:1
msgid "Window size"
msgstr "Vindustørrelse"

#: ../data/org.gnome.Maps.gschema.xml.h:2
msgid "Window size (width and height)."
msgstr "Vindustørrelse (bredde og høyde)."

#: ../data/org.gnome.Maps.gschema.xml.h:3
msgid "Window position"
msgstr "Vinduposisjon"

#: ../data/org.gnome.Maps.gschema.xml.h:4
msgid "Window position (X and Y)."
msgstr "Vinduposisjon (x og y)."

#: ../data/org.gnome.Maps.gschema.xml.h:5
msgid "Window maximized"
msgstr "Vindu maksimert"

#: ../data/org.gnome.Maps.gschema.xml.h:6
msgid "Window maximization state"
msgstr "Tilstand for maksimering av vindu"

#: ../data/org.gnome.Maps.gschema.xml.h:7
msgid "Maximum number of search results"
msgstr "Maksimalt antall søkeresultater"

#: ../data/org.gnome.Maps.gschema.xml.h:8
msgid "Maximum number of search results from geocode search."
msgstr "Maksimalt antall søkeresultater fra geocode-søk."

#: ../data/org.gnome.Maps.gschema.xml.h:9
msgid "Number of recent places to store"
msgstr "Antall nylige steder å lagre"

#: ../data/org.gnome.Maps.gschema.xml.h:10
msgid "Number of recently visited places to store."
msgstr "Antall nylig besøkte steder som skal lagres."

#: ../data/org.gnome.Maps.gschema.xml.h:11
msgid "Number of recent routes to store"
msgstr "Antall nylige ruter å lagre"

#: ../data/org.gnome.Maps.gschema.xml.h:12
msgid "Number of recently visited routes to store."
msgstr "Antall nylig besøkte ruter som skal lagres."

#: ../data/org.gnome.Maps.gschema.xml.h:13
msgid "Facebook check-in privacy setting"
msgstr "Personvern-innstilling for Facebook-innsjekk"

#: ../data/org.gnome.Maps.gschema.xml.h:14
msgid ""
"Latest used Facebook check-in privacy setting. Possible values are: "
"EVERYONE, FRIENDS_OF_FRIENDS, ALL_FRIENDS or SELF."
msgstr ""
"Sist brukt personvern-innstilling for Facebook-innsjekk. Følgende verdier er "
"gyldige: «EVERYONE» (alle), «FRIENDS_OF_FRIENDS» (venners venner), "
"«ALL_FRIENDS» (alle venner) og «SELF» (bare meg)."

#: ../data/org.gnome.Maps.gschema.xml.h:15
msgid "Foursquare check-in privacy setting"
msgstr "Personvern-innstilling for Foursquare-innsjekk"

#: ../data/org.gnome.Maps.gschema.xml.h:16
msgid ""
"Latest used Foursquare check-in privacy setting. Possible values are: "
"public, followers or private."
msgstr ""
"Sist brukt personvern-innstilling for Foursquare-innsjekk. Følgende verdier "
"er gyldige: «public», «followers» og «private»."

#: ../data/org.gnome.Maps.gschema.xml.h:17
msgid "Foursquare check-in Facebook broadcasting"
msgstr "Facebook-publisering av Foursquare-innsjekk"

#: ../data/org.gnome.Maps.gschema.xml.h:18
msgid ""
"Indicates if Foursquare should broadcast the check-in as a post in the "
"Facebook account associated with the Foursquare account."
msgstr ""
"Bestemmer om Foursquare skal publisere innsjekk som innlegg på Facebook-"
"kontoen som er koblet med aktuell Foursquare-konto."

#: ../data/org.gnome.Maps.gschema.xml.h:19
msgid "Foursquare check-in Twitter broadcasting"
msgstr "Twitter-publisering av Foursquare-innsjekk"

#: ../data/org.gnome.Maps.gschema.xml.h:20
msgid ""
"Indicates if Foursquare should broadcast the check-in as a tweet in the "
"Twitter account associated with the Foursquare account."
msgstr ""
"Bestemmer om Foursquare skal publisere innsjekk som innlegg på Twitter-"
"kontoen som er koblet med aktuell Foursquare-konto."

#: ../data/ui/app-menu.ui.h:1
msgid "About"
msgstr "Om"

#: ../data/ui/app-menu.ui.h:2
msgid "Quit"
msgstr "Avslutt"

#: ../data/ui/check-in-dialog.ui.h:1
msgid "Visibility"
msgstr "Synlighet"

#: ../data/ui/check-in-dialog.ui.h:2
msgid "Post on Facebook"
msgstr "Post på Facebook"

#: ../data/ui/check-in-dialog.ui.h:3
msgid "Post on Twitter"
msgstr "Post på Twitter"

#: ../data/ui/check-in-dialog.ui.h:4 ../data/ui/send-to-dialog.ui.h:2
msgid "_Cancel"
msgstr "_Avbryt"

#. Translators: Check in is used as a verb
#: ../data/ui/check-in-dialog.ui.h:5 ../data/ui/map-bubble.ui.h:5
msgid "C_heck in"
msgstr "S_jekk inn"

#: ../data/ui/check-in-dialog.ui.h:6
msgid "Everyone"
msgstr "Alle"

#: ../data/ui/check-in-dialog.ui.h:7
msgid "Friends of friends"
msgstr "Venner av venner"

#: ../data/ui/check-in-dialog.ui.h:8
msgid "Just friends"
msgstr "Bare venner"

#: ../data/ui/check-in-dialog.ui.h:9
msgid "Just me"
msgstr "Bare meg"

#: ../data/ui/check-in-dialog.ui.h:10
msgid "Public"
msgstr "Offentlig"

#: ../data/ui/check-in-dialog.ui.h:11
msgid "Followers"
msgstr "Følgere"

#: ../data/ui/check-in-dialog.ui.h:12
msgid "Private"
msgstr "Privat"

#: ../data/ui/context-menu.ui.h:1
msgid "What’s here?"
msgstr "Hva finnes her?"

#: ../data/ui/context-menu.ui.h:2
msgid "Copy Location"
msgstr "Kopier posisjon"

#: ../data/ui/location-service-notification.ui.h:1
msgid "Turn on location services to find your location"
msgstr "Skru på posisjonstjenester for å se hvor du er"

#: ../data/ui/location-service-notification.ui.h:2
msgid "Location Settings"
msgstr "Posisjonsinnstillinger"

#: ../data/ui/main-window.ui.h:2
msgid "Go to current location"
msgstr "Gå til gjeldende posisjon"

#: ../data/ui/main-window.ui.h:3
msgid "Choose map type"
msgstr "Velg type kart"

#: ../data/ui/main-window.ui.h:4
msgid "Toggle route planner"
msgstr "Slå av/på ruteplanlegger"

#: ../data/ui/main-window.ui.h:5
msgid "Toggle favorites"
msgstr "Slå av/på favoritter"

#: ../data/ui/main-window.ui.h:6
msgid "Maps is offline!"
msgstr "Kart er frakoblet!"

#: ../data/ui/main-window.ui.h:7
msgid ""
"Maps need an active internet connection to function properly, but one can't "
"be found."
msgstr ""
"Kart trenger en aktiv internettforbindelse for å fungere, men ingen ble "
"funnet."

#: ../data/ui/main-window.ui.h:8
msgid "Check your connection and proxy settings."
msgstr "Sjekk tilkobling og innstillinger for proxy."

#: ../data/ui/map-bubble.ui.h:1
msgid "Add to new route"
msgstr "Legg til i ny rute"

#: ../data/ui/map-bubble.ui.h:2
msgid "Open with another application"
msgstr "Åpne med et annet program"

#: ../data/ui/map-bubble.ui.h:3
msgid "Mark as favorite"
msgstr "Merk som favoritt"

#: ../data/ui/map-bubble.ui.h:6
msgid "Check in here"
msgstr "Sjekk inn her"

#: ../data/ui/search-popup.ui.h:1
msgid "Press enter to search"
msgstr "Trykk linjeskift for å søke"

#: ../data/ui/send-to-dialog.ui.h:1
msgid "Open location"
msgstr "Åpne posisjon"

#: ../data/ui/send-to-dialog.ui.h:3
msgid "_Open"
msgstr "_Åpne"

#: ../data/ui/sidebar.ui.h:1
msgid "Route search by GraphHopper"
msgstr "Rutesøk av GraphHopper"

#: ../data/ui/social-place-more-results-row.ui.h:1
msgid "Show more results"
msgstr "Vis flere resultater"

#: ../data/ui/user-location-bubble.ui.h:1 ../src/geoclue.js:209
msgid "Current location"
msgstr "Gjeldende posisjon"

#. To translators: %s can be "Unknown", "Exact" or "%f km²"
#: ../data/ui/user-location-bubble.ui.h:4
#, no-c-format
msgid "Accuracy: %s"
msgstr "Nøyaktighet: %s"

#: ../src/checkInDialog.js:176
msgid "Select an account"
msgstr "Velg en konto"

#: ../src/checkInDialog.js:181 ../src/checkInDialog.js:253
msgid "Loading"
msgstr "Laster"

#: ../src/checkInDialog.js:205
msgid "Select a place"
msgstr "Velg et sted"

#: ../src/checkInDialog.js:210
msgid ""
"Maps cannot find the place to check in to with Facebook. Please select one "
"from this list."
msgstr ""
"Kart finner ikke stedet som skal sjekkes inn på med Facebook. Velg et sted "
"her."

#: ../src/checkInDialog.js:212
msgid ""
"Maps cannot find the place to check in to with Foursquare. Please select one "
"from this list."
msgstr ""
"Kart finner ikke stedet som skal sjekkes inn på med Foursquare. Velg et sted "
"her."

#. Translators: %s is the name of the place to check in.
#.
#: ../src/checkInDialog.js:227
#, javascript-format
msgid "Check in to %s"
msgstr "Sjekk inn på %s"

#. Translators: %s is the name of the place to check in.
#.
#: ../src/checkInDialog.js:237
#, javascript-format
msgid "Write an optional message to check in to %s."
msgstr "Skriv en valgfri melding for å sjekk inn ved %s."

#: ../src/checkInDialog.js:289 ../src/checkIn.js:153
msgid "An error has occurred"
msgstr "Det har oppstått en feil"

#. Translators: %s is the place name that user wanted to check-in
#: ../src/checkIn.js:135
#, javascript-format
msgid "Cannot find \"%s\" in the social service"
msgstr "Fant ikke «%s» i sosial tjeneste"

#: ../src/checkIn.js:137
msgid "Cannot find a suitable place to check-in in this location"
msgstr "Fant ingen passende steder å sjekke inn"

#: ../src/checkIn.js:141
msgid ""
"Credentials have expired, please open Online Accounts to sign in and enable "
"this account"
msgstr ""
"Akkreditiver er utgått. Åpne Nettkontoer for å logge inn og bruke denne "
"kontoen"

#: ../src/mainWindow.js:315
msgid "Failed to connect to location service"
msgstr "Klarte ikke å koble til posisjonstjeneste"

#: ../src/mainWindow.js:320
msgid "Position not found"
msgstr "Fant ikke posisjonen"

#: ../src/mainWindow.js:365
msgid "translator-credits"
msgstr "Kjartan Maraas <kmaraas@gnome.org>"

#: ../src/mainWindow.js:368
msgid "A map application for GNOME"
msgstr "Et kartprogram for GNOME"

#: ../src/placeBubble.js:101
#, javascript-format
msgid "Postal code: %s"
msgstr "Postnummer: %s"

#: ../src/placeBubble.js:103
#, javascript-format
msgid "Country code: %s"
msgstr "Landskode: %s"

#: ../src/placeBubble.js:112
#, javascript-format
msgid "Population: %s"
msgstr "Folketall: %s"

#: ../src/placeBubble.js:115
#, javascript-format
msgid "Opening hours: %s"
msgstr "Åpningstider: %s"

#: ../src/placeBubble.js:120
msgid "Wikipedia"
msgstr "Wikipedia"

#: ../src/placeBubble.js:125
#, javascript-format
msgid "Wheelchair access: %s"
msgstr "Rullestol-vennlig: %s"

#: ../src/placeEntry.js:215
msgid "Failed to parse Geo URI"
msgstr "Klarte ikke å tolke Geo-URI"

#. Translators:
#. * This means wheelchairs have full unrestricted access.
#.
#: ../src/place.js:121
msgid "yes"
msgstr "ja"

#. Translators:
#. * This means wheelchairs have partial access (e.g some areas
#. * can be accessed and others not, areas requiring assistance
#. * by someone pushing up a steep gradient).
#.
#: ../src/place.js:128
msgid "limited"
msgstr "begrenset tilgang"

#. Translators:
#. * This means wheelchairs have no unrestricted access
#. * (e.g. stair only access).
#.
#: ../src/place.js:134
msgid "no"
msgstr "nei"

#. Translators:
#. * This means that the way or area is designated or purpose built
#. * for wheelchairs (e.g. elevators designed for wheelchair access
#. * only). This is rarely used.
#.
#: ../src/place.js:141
msgid "designated"
msgstr "spesialtilpasset"

#: ../src/routeService.js:90
msgid "No route found."
msgstr "Ingen rute funnet."

#: ../src/routeService.js:97
msgid "Route request failed."
msgstr "Forespørsel om rute feilet."

#: ../src/routeService.js:168
msgid "Start!"
msgstr "Start!"

#. Translators: %s is a time expression with the format "%f h" or "%f min"
#: ../src/sidebar.js:229
#, javascript-format
msgid "Estimated time: %s"
msgstr "Estimert tid: %s"

#: ../src/translations.js:55 ../src/translations.js:57
msgid "around the clock"
msgstr "hele døgnet"

#: ../src/translations.js:59
msgid "from sunrise to sunset"
msgstr "fra soloppgang til solnedgang"

#. Translators:
#. * This is a format string with two separate time ranges
#. * such as "Mo-Fr 10:00-19:00 Sa 12:00-16:00"
#. * The space between the format place holders could be
#. * substituted with the appropriate separator.
#.
#: ../src/translations.js:78
#, javascript-format
msgctxt "time range list"
msgid "%s %s"
msgstr "%s %s"

#. Translators:
#. * This is a format string with three separate time ranges
#. * such as "Mo-Fr 10:00-19:00 Sa 10:00-17:00 Su 12:00-16:00"
#. * The space between the format place holders could be
#. * substituted with the appropriate separator.
#.
#: ../src/translations.js:90
#, javascript-format
msgctxt "time range list"
msgid "%s %s %s"
msgstr "%s %s %s"

#. Translators:
#. * This is a format string consisting of a part specifying the days for
#. * which the specified time is applied and the time interval
#. * specification as the second argument.
#. * The space between the format place holders could be substituted with
#. * the appropriate separator or phrase and the ordering of the arguments
#. * can be rearranged with the %n#s syntax.
#: ../src/translations.js:121
#, javascript-format
msgctxt "time range component"
msgid "%s %s"
msgstr "%s %s"

#. Translators:
#. * This represents a format string consisting of two day interval
#. * specifications.
#. * For example:
#. * Mo-Fr,Sa
#. * where the "Mo-Fr" and "Sa" parts are replaced by the %s
#. * place holder.
#. * The separator (,) could be replaced with a translated variant or
#. * a phrase if appropriate.
#: ../src/translations.js:153
#, javascript-format
msgctxt "day interval list"
msgid "%s,%s"
msgstr "%s til %s"

#. Translators:
#. * This represents a format string consisting of three day interval
#. * specifications.
#. * For example:
#. * Mo-We,Fr,Su
#. * where the "Mo-We", "Fr", and "Su" parts are replaced by the
#. * %s place holder.
#. * The separator (,) could be replaced with a translated variant or
#. * a phrase if appropriate.
#: ../src/translations.js:167
#, javascript-format
msgctxt "day interval list"
msgid "%s,%s,%s"
msgstr "%s til %s og %s"

#: ../src/translations.js:186
msgid "every day"
msgstr "hver dag"

#. Translators:
#. * This represents a range of days with a starting and ending day.
#.
#: ../src/translations.js:198
#, javascript-format
msgctxt "day range"
msgid "%s-%s"
msgstr "%s-%s"

#: ../src/translations.js:209
msgid "public holidays"
msgstr "offentlige fridager"

#: ../src/translations.js:211
msgid "school holidays"
msgstr "skoleferie"

#. Translators:
#. * This is a list with two time intervals, such as:
#. * 09:00-12:00, 13:00-14:00
#. * The intervals are represented by the %s place holders and
#. * appropriate white space or connected phrase could be modified by
#. * the translation. The order of the arguments can be rearranged
#. * using the %n$s syntax.
#.
#: ../src/translations.js:251
#, javascript-format
msgctxt "time interval list"
msgid "%s, %s"
msgstr "%s, %s"

#: ../src/translations.js:265
msgid "not open"
msgstr "ikke åpen"

#. Translators:
#. * This is a time interval with a starting and an ending time.
#. * The time values are represented by the %s place holders and
#. * appropriate white spacing or connecting phrases can be set by the
#. * translation as needed. The order of the arguments can be rearranged
#. * using the %n$s syntax.
#.
#: ../src/translations.js:280
#, javascript-format
msgctxt "time interval"
msgid "%s-%s"
msgstr "%s-%s"

#. Translators: Accuracy of user location information
#: ../src/utils.js:256
msgid "Unknown"
msgstr "Ukjent"

#. Translators: Accuracy of user location information
#: ../src/utils.js:259
msgid "Exact"
msgstr "Nøyaktig"

#: ../src/utils.js:354
#, javascript-format
msgid "%f h"
msgstr "%f t"

#: ../src/utils.js:356
#, javascript-format
msgid "%f min"
msgstr "%f min"

#: ../src/utils.js:358
#, javascript-format
msgid "%f s"
msgstr "%f s"

#. Translators: This is a distance measured in kilometers
#: ../src/utils.js:369
#, javascript-format
msgid "%f km"
msgstr "%f km"

#. Translators: This is a distance measured in meters
#: ../src/utils.js:372
#, javascript-format
msgid "%f m"
msgstr "%f m"

#. Translators: This is a distance measured in miles
#: ../src/utils.js:380
#, javascript-format
msgid "%f mi"
msgstr "%f mil"

#. Translators: This is a distance measured in feet
#: ../src/utils.js:383
#, javascript-format
msgid "%f ft"
msgstr "%f fot"
